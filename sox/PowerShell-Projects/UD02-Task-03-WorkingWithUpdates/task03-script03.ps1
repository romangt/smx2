﻿# The script search if the module PSWindowsUpdate is installed, if not
# It will start with the installation.

# We did not include the option -AutoReboot at the end of the last line 
# because this is only for testing purposes. But is convenient to reboot
# after the updates.




$windows = (Get-installedModule)

$windows | ForEach-Object {
    

    if ( $_.Name -ne "PSWindowsUpdate" ) {
    Install-Module PSWindowsUpdate
    }
    }


Write-Host "Depending on the machine, it can take few seconds in order to start the updates..."
Write-Host "Be patient!"

Get-WindowsUpdate 
Install-WindowsUpdate -IgnoreUserInput -acceptall -ForceInstall

exit 0