﻿#Almighty Raul Script04

$http = (Get-ItemProperty HKCU:\SOFTWARE\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice)

$https = (Get-ItemProperty HKCU:\SOFTWARE\Microsoft\Windows\Shell\Associations\UrlAssociations\https\UserChoice)

# Next lines checks if "firefox" is default for http and https
# If one of them is not "true" then it will show " * is not the default browser

$firefox=0
($prueba= $http |Select-String "firefox*") -or ($firefox= 1) | Out-Null
$firefox1=0
($prueba= $https |Select-String "firefox*") -or ($firefox1= 1) | Out-Null


$edge=0
($prueba= $http |Select-String "edge*") -or ($edge= 1) | Out-Null
$edge1=0
($prueba= $http |Select-String "edge*") -or ($edge1= 1) | Out-Null

$chrome=0
($prueba= $http |Select-String "chrome*") -or ($chrome= 1) | Out-Null
$chrome1=0
($prueba= $http |Select-String "chrome*") -or ($chrome1= 1) | Out-Null


if ( $firefox -eq 1 -and $firefox1 -eq 1)
{
Write-Host " * Mozilla Firefox is not the default browser"
}


if ( $edge -eq 1 -and $edge1 -eq 1)
{
Write-Host " * Edge is not the default browser"
}

if ( $chrome -eq 1 -and $chrome1 -eq 1)
{
Write-Host " * Google Chrome is not the default browser"
}

exit 0