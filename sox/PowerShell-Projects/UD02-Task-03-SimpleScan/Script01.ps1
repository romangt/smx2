﻿#Almighty Raul Script

$programas=Get-ItemProperty HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, DisplayVersion

# Check if scite is installed

$rc= 0
($scite= $programas |Select-String "scite*") -or ($rc= 1) | Out-Null

if ( $rc -eq 0 )
{
Write-Host " * Scite is installed"
}
else{
Write-Host " * Scite is NOT installed"
exit
}

# Check if the version is above 5.0

$scite = ($programas | Select-String "scite.*")
$CharArray = $scite -Split("=")
$final = ($CharArray[2]) 
$cut = $final -split ("\.") 
$CharArray2 = ($cut[0])


if ( $CharArray2 -ge 5 )
{
Write-Host " * Everything is fine ^_^ "
}
else{
Write-Host " * WARNING Vulnerability Detected"
}

exit 0