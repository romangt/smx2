﻿#Almighty Raul Script02

$programas=Get-ItemProperty HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, DisplayVersion

#Absolute path where the .msi file must be placed "c:\Users"
$msiSciteInstaller = '/i', "C:\Users\scite-5.3.1x64.msi", '/qb!'



# Check if scite is installed

$rc= 0
($scite= $programas |Select-String "scite*") -or ($rc= 1) | Out-Null

if ( $rc -eq 0 )
{
Write-Host " * Scite is installed"
}
else{
Write-Host " * Scite is NOT installed, INSTALLING Scite..."
msiexec.exe @msiSciteInstaller
}

# Check if the version is above 5.0

$scite = ($programas | Select-String "scite.*")
$CharArray = $scite -Split("=")
$final = ($CharArray[2]) 
$cut = $final -split ("\.") 
$CharArray2 = ($cut[0])


if ( $CharArray2 -ge 5 )
{
Write-Host " * Everything is fine, last version of scite is installed "
}
else{
Write-Host " * WARNING Vulnerability Detected, INSTALLING Scite..."
msiexec.exe @msiSciteInstaller
}

exit 0