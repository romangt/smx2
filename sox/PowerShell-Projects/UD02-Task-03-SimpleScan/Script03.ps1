﻿#Almighty Raul Script03

$programas=Get-ItemProperty HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\* | Select-Object DisplayName, DisplayVersion

#Absolute path where the .msi file must be placed "c:\Users"
$msiSciteInstaller = '/i', "C:\Users\scite-5.3.1x64.msi", '/qb!'
$msiSciteUninstaller = '/x', "C:\Users\scite-5.3.1x64.msi", '/qb!'


# Asking to the user if scite is already installed

$installed = Read-Host "Is scite installed in your computer? (y/n)"

if ( $installed -like "y" )
{
Write-Host " * Scite is installed"
}
else{
Write-Host " * Scite is NOT installed, INSTALLING Scite..."
msiexec.exe @msiSciteInstaller
exit
}

# Asking to the user if the version is above 5.0

$version = Read-Host "Is scite version 5.0 or above? (y/n)"


if ( $version -like "y" )
{
Write-Host " * Everything is fine, last version of scite is installed "
}
else{
Write-Host " * WARNING Vulnerability Detected, Removing previous version..."
msiexec.exe @msiSciteUninstaller
Write-Host " * Installing version scite-5.3.1x64..."
msiexec.exe @msiSciteInstaller
}

exit 0