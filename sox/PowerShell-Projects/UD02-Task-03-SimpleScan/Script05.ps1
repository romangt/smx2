﻿# The following lines checks what is the current default browser
# if Edge is not the default one, it will change it

$item = (Get-ItemProperty HKCU:\Software\Microsoft\Windows\Shell\Associations\UrlAssociations\https\UserChoice)

$defaultbrowserfound = ($item.ProgID)

$path1 = 'HKCU:\Software\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice'
$path2 = 'HKCU:\Software\Microsoft\Windows\Shell\Associations\UrlAssociations\https\UserChoice'

if ( $defaultbrowserfound -ne "MSEdgeHTM" ){
    
    Set-ItemProperty $path1 -Name ProgID -Value 'MSEdgeHTM'
    Set-ItemProperty $path2 -Name ProgID -Value 'MSEdgeHTM'
    Write-Host " Default browser is $defaultbrowserfound *Setting Edge as default browser"

    }
    else{
        Write-Host " *Default browser is already Edge"
        }

    exit 0
    