﻿[int]$argumento=$args[0]

if ($argumento -eq 1 ){
    Write-Host "* The chosen variable is USERPROFILE"
    Write-Host "* You can use to access: `$Env:USERPROFILE"
    Write-Host "* Return the current user profile path"
    Write-Host "* - To find current user profile: `$Env:USERPROFILE"
    }
if ($argumento -eq 2 ){
    Write-Host "* The chosen variable is windir"
    Write-Host "* You can use to access: `$Env:windir"
    Write-Host "* Shows the current Windows installation path"
    Write-Host "* - To find current Windows directory: `$Env:windir"
    }
if ($argumento -eq 3 ){
    Write-Host "* The chosen variable is OS"
    Write-Host "* You can use to access: `$Env:OS"
    Write-Host "* Shows the current Windows version"
    Write-Host "* - To find current Windows version: `$Env:OS"
    }
if ($argumento -eq 4 ){
    Write-Host "* The chosen variable is Home Path"
    Write-Host "* You can use to access: `$Env:HOMEPATH"
    Write-Host "* Shows the current home path"
    Write-Host "* - To move to the current home path: `cd $Env:HOMEPATH"
    }
if ($argumento -eq 5 ){
    Write-Host "* The chosen variable is Program Files"
    Write-Host "* You can use to access: `$Env:ProgramFiles"
    Write-Host "* Shows the current Windows Programs path"
    Write-Host "* - Lists apps installed by Windows: `ls $env:ProgramFiles"
    }

    exit 0