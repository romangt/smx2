#!/bin/bash

usuario=$(whoami)
echo $usuario

grupo=$(ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://ubuntusrv.smx2023.net | grep $usuario | sed '2!d' | cut -d "," -f2)

if [ $grupo = "cn=goblins" ]; then
	echo "el usuario pertenece al grupo $grupo"
fi

exit 0


