#!/bin/bash

cat MOCK_DATA.csv | while read linea; do

	id01=$(echo $linea | cut -d "," -f1)
	rc=0
	username=$(echo $linea | cut -d "," -f8 | grep ^f) || rc=1
	plant=$(echo $linea | cut -d "," -f9)
	dos=2
	seis=6

	let resto=$id01%$dos
	let multiplo=$id01%$seis


	if [ $resto -ge 1 ] && [ $rc -eq 0 ] || [ $multiplo -eq 0 ] && [ $rc -eq 0 ]; then
		echo "ID: $id01 USERNAME: $username PLANT: $plant is odd and 6-multiple"
	fi	


done














exit 0
