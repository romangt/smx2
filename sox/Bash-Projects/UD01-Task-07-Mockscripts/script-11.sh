#!/bin/bash

sum=0

cat MOCK_DATA.csv | cut -d "," -f5 | grep -v "ip_address" | while IFS=. read ip1 ip2 ip3 ip4; do
	
	IP1=$(echo "obase=2;$ip1" | bc)
	IP2=$(echo "obase=2;$ip2" | bc)
	IP3=$(echo "obase=2;$ip3" | bc)
	IP4=$(echo "obase=2;$ip4" | bc)
	#doc=$(cat MOCK_DATA.csv)
	#id01=$(echo $doc | cut -d "," -f1)

#Hasta aqui saca las IP en formato binario


#A partir de aquí cuenta los 1s, los suma y nos dice si la suma es numero primo
 
	let sum=$sum+1

	var1=$(echo $IP1 | tr -cd '1' | wc -c) 
	var2=$(echo $IP2 | tr -cd '1' | wc -c)
	var3=$(echo $IP3 | tr -cd '1' | wc -c)
	var4=$(echo $IP4 | tr -cd '1' | wc -c)


	let suma2=$var1+$var2+$var3+$var4

	
	DIVISOR=2

	ESPRIMO="si"
	
	while [ $DIVISOR -lt $suma2 ]; do
		
		let RESTO=$suma2%$DIVISOR
		if [ $RESTO -eq 0 ]; then
			ESPRIMO="no"
		fi	
		let DIVISOR=$DIVISOR+1	
	done

	if [ $ESPRIMO = "si" ]; then
		echo "Usuario $sum IP: $ip1.$ip2.$ip3.$ip4 la suma de 1s de la IP es primo"
	fi
	



	


done





exit 0
