#!/bin/bash


cat MOCK_DATA.csv | while read linea; do 
	auxId=$(echo $linea | cut -d "," -f1)
	auxUsername=$(echo $linea |cut -d "," -f8)

	# UN NUMERO ES PRIMO SI NO EXISTE NINGUN NUMERO
	# ENTRE EL MISMO Y EL UNO QUE SEA DIVISOR ENTERO = RESTO SEA 0

	DIVISOR=2

	ESPRIMO="si"
	
	while [ $DIVISOR -lt $auxId ]; do
		#echo " $auxId : Probando con $DIVISOR "
		let RESTO=$auxId%$DIVISOR
		if [ $RESTO -eq 0 ]; then
			ESPRIMO="no"
		fi
		let DIVISOR=$DIVISOR+1	
	done

	if [ $ESPRIMO = "si" ]; then
		echo " Id $auxId, Username : $auxUsername"
	fi


done


exit 0


