#!/bin/bash

cat MOCK_DATA.csv | while read linea; do
	username=$(echo $linea | cut -d "," -f8)
	ipaddress=$(echo $linea | cut -d "," -f5)
	ipclass=$(echo $linea | cut -d "," -f5 | cut -d "." -f1)
	
	if [ $ipclass -le 127 ]; then
		echo "Username: $username The address $ipaddress is class A"
	else
		if [ $ipclass -gt 127 ] && [ $ipclass -lt 192 ]; then
			echo "Username: $username The address $ipaddress is class B"
		fi
	fi



done






exit 0
