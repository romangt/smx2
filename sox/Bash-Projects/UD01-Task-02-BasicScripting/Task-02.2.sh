#!/bin/bash

total=0
dos=2
trece=13

for i in {1..301}

do

let div1=$i%$dos

if [ $div1 -eq 0 ]; then

let div2=$i%$trece

if [ $div2 -eq 0 ]; then

let total=$i+$total

fi

fi

done

echo "la suma de los pares y multiplos de 13 es $total"

exit 0
