#!/bin/bash



a=$(ip a | grep inet | cut -d " " -f6 | sed -n 3p | cut -d "." -f1)

b=$(ip a | grep inet | cut -d " " -f6 | sed -n 3p | cut -d "." -f3)

GTW="INVALIDO"


if [ $a -eq "192" ] && [ $b -eq "1" ]; then

	GTW="192.168.1.254"
	echo "Red encontrada"
fi
#--------------------------------------------------


if [ $a -eq "192" ] && [ $b -eq "2" ]; then

	GTW="192.168.2.254"

        echo "Red encontrada"
fi
#---------------------------------------------------


if [ $a -eq "192" ] && [ $b -eq "4" ]; then

	GTW="192.168.4.254"

        echo "Red encontrada"
fi
#---------------------------------------------------


if [ $a -eq "10" ]; then

	GTW="10.2.2.254"

        echo "Red encontrada"
fi


#---------------------------------------------------
if [ $a -eq "172" ]; then

	GTW="172.29.0.1"

        echo "Red encontrada"
fi


#--------------------------------


if [ ${GTW} = "INVALIDO" ]; then

	echo " RED DESCONOCIDA "

else

	ping -c5 ${GTW}

fi
exit

