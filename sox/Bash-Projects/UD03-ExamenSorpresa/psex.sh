#!/bin/bash

rm -r salida

ps -ex | tr -s " " | cut -d " " -f5 | sed "1d" >> salida

tminutos=0
thoras=0
sumamins=0
sumadias=0

cat salida | while read line; do
	
	minutos=$(echo $line | cut -d ":" -f2)
	horas=$(echo $line | cut -d ":" -f1)
	tminutos=$(echo "$tminutos + $minutos" | bc)
	thoras=$(echo "$thoras + $horas" | bc)
	echo $tminutos > Tminutos	
	echo $thoras > Thoras

done
	tminutos=$(cat Tminutos)
	thoras=$(cat Thoras)


	while [ $tminutos -ge 60 ]; do
		let sumamins=$sumamins+1
		let tminutos=$tminutos-60
	done
	
	while [ $thoras -ge 24 ]; do
		let sumadias=$sumadias+1
		let thoras=$thoras-24
	done

	let totalh=$thoras+$sumamins


	echo "$sumadias dias, $totalh horas, $tminutos minutos"

	

exit 0
