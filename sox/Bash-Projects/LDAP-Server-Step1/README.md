## Tarea: Instalación de OpenLDAP

Instalamos:

- sudo apt install slapd
- sudo apt search ldapadmin
- sudo apt install mysql-server
- sudo apt install phpldapadmin

Accedemos al navegador del cliente:
http://HERE-MY-IP/phpldapadmin/

Si hacemos port-forwarding (si por ejemplo usamos red nat) tendríamos que añadir el puerto:
http://HERE-MY-IP:2023/phpldapadmin/

Solucionar error de memory limit: editar la linea memory_limit en /etc/php/8.1/apache2/php.ini de 128 a 512M 

![captura](./1.png "captura")

Para parchear el phpldapadmin debemos hacer wget del siguiente [zip](https://github.com/leenooks/phpLDAPadmin/releases/tag/1.2.6.4), y ejecutar unzip. Una vez extraído el contenido, eliminamos con rm –r /usr/share/phpldapadmin/* y copiamos lo que hemos descomprimido con cp -r  

A tener en cuenta que nos fallará porque hemos eliminado el conf.php, por tanto, simplemente hacemos cp de /usr/share/phpldapadmin/config/config.php.example y se renombra a config.php 

>> A tener en cuenta. Si durante el dpkg-reconfigure no nos deja continuar porque aparece el siguiente error (invoke-rc.d: policy-rc.d denied execution of start) podemos solucionarlo editando el script (/usr/sbin/policy-rc.d) cambiando exit 101 a exit 0. [Fuente](https://askubuntu.com/questions/365911/why-the-services-do-not-start-at-installation)

Hacemos reconfigure: sudo dpkg-reconfigure slapd

En esta pregunta nos indica qué debe hacer el gestor de software si
decidimos borrar el paquete, lo que marcaremos será NO Borrar la
base de datos en caso de desinstalación del servicio slapd para
posibles recuperaciones frente a desastres.
Esto nos marca también que si queremos borrar la base de datos tendremos
que realizar esta operación manualmente, y no nos servirá el proceso de
desinstalación del servidor de LDAP.
Por último, nos pregunta si deseamos mover la base de datos antigua (que
está vacia en este momento) durante este proceso. Optaremos por la
opción: Sí

Una vez realizado todo el proceso anterior, si volvemos a la pantalla de
phpLDAPadmin, veremos que utilizando un dn de administrador nos
permite entrar en el servidor.
cn=admin,dc=ubuntusrvXX,dc=smx2023,dc=net

Sin embargo, aunque la autenticación ha tenido éxito, no podemos
administrar nuestro dominio, ya que nos aparece en la parte derecha para
administrar el dominio:
example.com
que No funciona.
Esto es debido a que por defecto el phpLDAPadmin administra ese
dominio. Deberemos ir al fichero de configuración y cambiar la base de
la búsqueda.
El fichero de configuración se encuentra en :
/etc/phpldapadmin/config.php

![MarineGEO circle logo](https://gitlab.com/aberlanas/SMX-SOX/-/raw/master/Unit04-DomainAdministration/imgs/slapd-10.png "MarineGEO logo")

Creamos los objetos usando el interfaz gráfico:

![captura](./3.png "captura")

Las ldap-utils pueden ser instaladas en cualquier máquina para
realizar consultas.
sudo apt install ldap-utils
Una vez realizada la instalación, podemos comprobar que somos capaces de
contactar con el ldap server utilizando la línea de comandos:
ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://ubuntusrv.smx2023.net 

![captura](./2.png "captura")
