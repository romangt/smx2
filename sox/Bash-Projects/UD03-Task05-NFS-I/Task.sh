#!/bin/bash

# syslog can be found in /var/log/syslog 

# checking if lan is mounted

rc=0
lan=$(mount | grep -w "lan") || rc=1

if [ $rc -eq 1 ]; then
	sudo mount 10.0.2.15:/srv/nfs/exportado-lan /client/lan
	logger FOUND /client/lan UNMOUNTED
	logger MOUNTED /client/lan
fi


# checking if todos is mounted

rc=0
todos=$(mount | grep -w "todos") || rc=1

if [ $rc -eq 1 ]; then
        sudo mount 10.0.2.15:/srv/nfs/exportado-todos /client/todos
	logger FOUND /client/todos UNMOUNTED
	logger MOUNTED /client/todos
fi





exit 0