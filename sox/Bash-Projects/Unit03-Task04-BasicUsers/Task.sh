#!/bin/bash

# if less than 2 arguments then error
if [ $# -lt 2 ]; then
    echo " Not enough arguments "
    exit 1
fi

# checking and filtering if valid user exists

rc=0
cat /etc/passwd | grep -wq $1 || rc=1



if [ $rc -eq 0 ]; then
user=$(cat /etc/passwd | grep $1 | cut -d ":" -f1)
# REPLENISH MODULE
    if [ $2 = "replenish" ]; then
        
        mkdir /home/$user/Workspace
        mkdir /home/$user/Workspace/config
        mkdir /home/$user/Workspace/bin
        mkdir /home/$user/Workspace/source
        mkdir /home/$user/Workspace/rsrc
        echo $user > /home/$user/Workspace/config/sample.txt
        echo $user > /home/$user/Workspace/bin/sample.txt
        echo $user > /home/$user/Workspace/source/sample.txt
        echo $user > /home/$user/Workspace/rsrc/sample.txt

        exit 0


    fi

# TEST MODULE
    if [ $2 = "test" ]; then
        if [ $# -eq 3 ]; then
            if [ -d $3 ]; then
                path= ls -R $3
                    echo $path | while read line; do
                    if [ -r $line ]; then
                        echo "$line exists and could be read by $1"
                        done
                    fi

                    done
                    
                    exit 0
                fi




            else
                echo "* Error! - The folder given to test not exists"
                exit 1
            fi

        fi

    fi

# CLEAN MODULE

     if [ $2 = "clean" ]; then
        if [ $# -eq 3 ]; then
            rm -r $3
            exit 0
        fi

    fi

else

echo " * Error - The User given not exists"
exit 1

fi

exit 0