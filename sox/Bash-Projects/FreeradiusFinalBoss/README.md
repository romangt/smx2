Configurar freeradius para que loguee con usuarios del servidor ldap 

Fuentes: https://www.golinuxcloud.com/freeradius-ldap-authentication-authorization/  

https://askubuntu.com/questions/708663/radius-is-ignoring-request-to-authentication-address  

 

Captura del mods enabled:  

 ![captura](./capturas/Captura%20desde%202023-02-24%2017-32-19.png "captura")

Captura del clients.conf: 

![captura](./capturas/Captura%20desde%202023-02-24%2017-34-40.png "captura")

Comprobar si freeradius encuentra usuarios ldap (usar ip del cliente radius para el test) 

![captura](./capturas/Captura%20desde%202023-02-24%2017-36-08.png "captura")

  

Instalando punto de acceso wifi (hostapd) 

Fuentes: https://askubuntu.com/questions/180733/how-to-create-a-wi-fi-hotspot-in-access-point-mode/1133533#1133533  

Solucionar error hostapd masked: https://forums.raspberrypi.com/viewtopic.php?t=235598  

 

sudo lsof -i :1812 

sudo kill -9 8679 

 

Netplan: 

![captura](./capturas/Captura%20desde%202023-02-24%2017-37-50.png "captura")

Le estamos dando una ip fija 175.220 

Y la puerta de enlace la del servidor del aula 4.254 (le decimos que se sale a internet por esta ip) 

Ip r nos indica la ip de la tarjeta de red (si se ha asignado o no) 

 
![captura](./capturas/Captura%20desde%202023-02-24%2017-39-03.png "captura")
 

Añadir regla iptables:  

 
![captura](./capturas/Captura%20desde%202023-02-24%2017-40-10.png "captura")
 

 

 

En dnsmasq descomentamos la ruta que da al directorio de configuración: 

![captura](./capturas/Captura%20desde%202023-02-24%2017-41-18.png "captura")

 ![captura](./capturas/Captura%20desde%202023-02-24%2017-42-17.png "captura")

Dnsmasq hace de dhcp dand un rango de ips que hemos indicado 

Los server son los dns de conselleria 

La última línea indica la Mac de wlo1 (el wifi del portátil), radius es el nombre del portátil (etc/hosts), y la ip 