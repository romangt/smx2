#!/bin/bash
# Author: Sergio Garcia Lerida & Raul Roman
# THE NAME OF THIS SCRIPT IS BU FOR BACKUP


fecha=$(date +%d-%m-%Y)

#Important: We do NOT execute the script with sudo anymore. Seems there was some changes and now we only need to add sudo inside the script lines, and in order to execute the script, we do not need to add sudo

sudo mkdir -p /home/$USER/$fecha

sudo cp /etc/netplan/01-network-manager-all.yaml /home/$USER/$fecha
sudo cp /etc/freeradius/3.0/clients.conf /home/$USER/$fecha
sudo cp /etc/dnsmasq.conf /home/$USER/$fecha
sudo cp /etc/dnsmasq.d/raultodopoderoso.conf /home/$USER/$fecha
sudo cp /etc/hostapd/hostapd.conf /home/$USER/$fecha
sudo cp /etc/freeradius/3.0/mods-enabled/ldap /home/$USER/$fecha
sudo cp /etc/apache2/sites-enabled/000-default.conf /home/$USER/$fecha
sudo cp /etc/freeradius/3.0/sites-enabled/ldapserver /home/$USER/$fecha
sudo cp /etc/sysctl.conf /home/$USER/$fecha
sudo cp /etc/network/interfaces /home/$USER/$fecha





exit 0
