#!/bin/bash


#checking if user is root

if [ "$EUID" -ne 0 ]; then
	echo " Please run as root"
	exit
fi

#checking if mounting points exists, if not, they will be created

if ! [ -d /mnt/samba/burundi ]; then
	echo " Mount point burundi does not exists, creating..."
	mkdir -p /mnt/samba/burundi
fi


if ! [ -d /mnt/samba/cameroon ]; then
        echo " Mount point cameroon does not exists, creating..."
        mkdir -p /mnt/samba/cameroon
fi

if ! [ -d /mnt/samba/guinea ]; then
        echo " Mount point guinea does not exists, creating..."
        mkdir -p /mnt/samba/guinea
fi

#checking if .credentials is created, if not, it will be created

if ! [ -f ./.credentials ]; then
	echo " Credentials file does not exists, creating..."
	echo -e "username=raul\npassword=Rom1212 " >> .credentials
	chmod 0600 .credentials
fi

#mounting directories with credentials
#for some reason, .credentials is not being useful, so we can mount manually using the
#following lines

sudo mount.cifs //10.0.2.15/nile /mnt/samba/burundi -o user=raul,password=Rom1212
sudo mount.cifs //10.0.2.15/congo /mnt/samba/cameroon -o user=raul,password=Rom1212
sudo mount.cifs //10.0.2.15/niger /mnt/samba/guinea -o user=raul,password=Rom1212

#sudo mount.cifs //10.0.2.15/nile /mnt/samba/burundi -o credentials=/home/raul/examrivers/.credentials
#sudo mount.cifs //10.0.2.15/congo /mnt/samba/cameroon -o credentials=/home/raul/examrivers/.credentials
#sudo mount.cifs //10.0.2.15/niger /mnt/samba/guinea -o credentials=/home/raul/examrivers/.credentials


#if everything is correct, message will display the lines for fstab

if [ -f /samba/burundi/examplenile.txt.txt ] && [ -f /samba/cameroon/examplecongo.txt.txt ] && [ -f /samba/guinea/exampleniger.txt.txt ]; then
	echo " Everything is correct and the lines to use in fstab would be: "
	echo " //10.0.2.15/nile /mnt/samba/burundi cifs credentials=./.credentials 0 0 "
	echo " //10.0.2.15/congo /mnt/samba/cameroon cifs credentials=./.credentia
ls 0 0 "
	echo " //10.0.2.15/niger /mnt/samba/guinea cifs credentials=./.credentia
ls 0 0 "

fi




exit 0
