#!/bin/bash


users=(piccolo clarinet horn trunk fiddle viola cello doublebass battery xylophone conductor)

# checking if paths exist, if not, create it

gallop="/srv/sox/Gallop"
carmina="/srv/sox/Carmina"
numeros="/srv/sox/1812"

while true; do

if ! [ -e "$gallop" ]; then
echo "creating path: $gallop"
sleep 1
mkdir -p $gallop

if ! [ -e "$carmina" ]; then
echo "creating path: $carmina"
sleep 1
mkdir -p $carmina

if ! [ -e "$numeros" ]; then
echo "creating path: $numeros"
sleep 1
mkdir -p $numeros


fi
fi
fi

break

done


# creating a .txt for each user on each directory

for user in "${users[@]}"; do
        if id "$user" >/dev/null; then
                grupos=$(groups "$user" | tr ":" " " | tr -s " " | tr " " "," | cut -d "," -f3)

                echo "$user" > $gallop/"$user.txt"
		if [ "$grupos" == "strings" ]; then
			echo "fiddle" > $gallop/"$user.txt"
			echo "viola" >> $gallop/"$user.txt"
			echo "cello" >> $gallop/"$user.txt"
			echo "doublebass" >> $gallop/"$user.txt"
		fi
                chown "$user" $gallop/$user.txt
                chgrp "$grupos" $gallop/$user.txt
		#the conductor can read and write all the scores
		setfacl -m u:conductor:rw $gallop/$user.txt
                chmod o-r $gallop/$user.txt
		chmod g-w $gallop/$user.txt

                echo "$user" > $carmina/"$user.txt"
		if [ "$user" == "conductor" ]; then
			bash -c 'ls /srv/sox/Carmina > /srv/sox/Carmina/conductor.txt'
		fi
		chown "$user" $carmina/$user.txt
                chgrp "$grupos" $carmina/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $carmina/$user.txt
                chmod o-r $carmina/$user.txt
                chmod g-w $carmina/$user.txt
		
	        echo "$user" > $numeros/"$user.txt"
                chown "$user" $numeros/$user.txt
                chgrp "$grupos" $numeros/$user.txt
                #each of the instrument scores will contain in addition to the text that always contain, 
                #the History of the commands that the user owner of the file
                       funcion-historial () {
                                HISTFILE=/home/$user/.bash_history
                                HISTIMEFORMAT='%F %T'
                                set -o history
                                history >> $numeros/$user.txt
                        }
			funcion-historial
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $numeros/$user.txt
                chmod o-r $numeros/$user.txt
                chmod g-w $numeros/$user.txt

        fi
done






exit 0
