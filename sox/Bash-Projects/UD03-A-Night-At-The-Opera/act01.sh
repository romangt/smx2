#!/bin/bash

users=(piccolo clarinet horn trunk fiddle viola cello doublebass battery xylophone conductor)


# checking if paths exist, if not, create it

kiev="/srv/sox/TheGreatGateOfKiev"
danube="/srv/sox/BlueDanube"
world="/srv/sox/NewWorldSymphonie"
jazz="/srv/sox/TheJazzSuite"

while true; do

if ! [ -e "$kiev" ]; then
echo "creating path: $kiev"
sleep 1
mkdir -p $kiev

if ! [ -e "$danube" ]; then
echo "creating path: $danube"
sleep 1
mkdir -p $danube

if ! [ -e "$world" ]; then
echo "creating path: $world"
sleep 1
mkdir -p $world

if ! [ -e "$jazz" ]; then
echo "creating path: $jazz"
sleep 1
mkdir -p $jazz



fi
fi
fi
fi

break

done




# creating a .txt for each user on each directory

for user in "${users[@]}"; do
	if id "$user" >/dev/null; then
		grupos=$(groups "$user" | tr ":" " " | tr -s " " | tr " " "," | cut -d "," -f3)

		echo "$user" > $kiev/"$user.txt"
		chown "$user" $kiev/$user.txt
	      	chgrp "$grupos" $kiev/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $kiev/$user.txt
                chmod o-r $kiev/$user.txt
                chmod g-w $kiev/$user.txt

		echo "$user" > $danube/"$user.txt"
		chown "$user" $danube/$user.txt
		chgrp "$grupos" $danube/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $danube/$user.txt
                chmod o-r $danube/$user.txt
                chmod g-w $danube/$user.txt

		echo "$user" > $world/"$user.txt"
		chown "$user" $world/$user.txt
                chgrp "$grupos" $world/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $world/$user.txt
                chmod o-r $world/$user.txt
                chmod g-w $world/$user.txt

		echo "$user" > $jazz/"$user.txt"
		chown "$user" $jazz/$user.txt
                chgrp "$grupos" $jazz/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $jazz/$user.txt
                chmod o-r $jazz/$user.txt
                chmod g-w $jazz/$user.txt


	fi
done




exit 0
