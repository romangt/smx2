#!/bin/bash

users=(piccolo clarinet horn trunk fiddle viola cello doublebass battery xylophone conductor)


# checking if paths exist, if not, create it

nocturns="/srv/sox/Nocturns"
fratres="/srv/sox/Fratres"
adagio="/srv/sox/Adagio"
deprofundis="/srv/sox/DeProfundis"

while true; do

if ! [ -e "$nocturns" ]; then
echo "creating path: $nocturns"
sleep 1
mkdir -p $nocturns

if ! [ -e "$fratres" ]; then
echo "creating path: $fratres"
sleep 1
mkdir -p $fratres

if ! [ -e "$adagio" ]; then
echo "creating path: $adagio"
sleep 1
mkdir -p $adagio


if ! [ -e "$deprofundis" ]; then
echo "creating path: $deprofundis"
sleep 1
mkdir -p $deprofundis



fi
fi
fi
fi

break

done

# creating a .txt for each user on each directory

for user in "${users[@]}"; do
        if id "$user" >/dev/null; then
                grupos=$(groups "$user" | tr ":" " " | tr -s " " | tr " " "," | cut -d "," -f3)

                echo "$user" > $nocturns/"$user.txt"
                chown "$user" $nocturns/$user.txt
                chgrp "$grupos" $nocturns/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $nocturns/$user.txt
                chmod o-r $nocturns/$user.txt
                chmod g-w $nocturns/$user.txt
		touch -d "01/01/1977" $nocturns/$user.txt	
	
                echo "$user" > $fratres/"$user.txt"
                chown "$user" $fratres/$user.txt
                chgrp "$grupos" $fratres/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $fratres/$user.txt
                chmod o-r $fratres/$user.txt
                chmod g-w $fratres/$user.txt

                echo "$user" > $adagio/"$user.txt"
                chown "$user" $adagio/$user.txt
                chgrp "$grupos" $adagio/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $adagio/$user.txt
                chmod o-r $adagio/$user.txt
                chmod g-w $adagio/$user.txt

		echo "$user" > $deprofundis/"$user.txt"
		# in deprofundis each score must contain username+uid+gid
uid=$(id $user | tr "=" " " | tr "(" " " | tr ")" " " | tr "," " " | tr -s " " | cut -d " " -f2)
gid=$(id $user | tr "=" " " | tr "(" " " | tr ")" " " | tr "," " " | tr -s " " | cut -d " " -f5)
                echo "$uid" >> $deprofundis/$user.txt
                echo "$gid" >> $deprofundis/$user.txt
                chown "$user" $deprofundis/$user.txt
                chgrp "$grupos" $deprofundis/$user.txt
                #the conductor can read and write all the scores
                setfacl -m u:conductor:rw $deprofundis/$user.txt
                chmod o-r $deprofundis/$user.txt
                chmod g-w $deprofundis/$user.txt

        fi
done


# setting permissions to new files created. All users must be
# able to create new files and those automatically will belong                 # to orchestra   
chmod -R g+s $fratres



# creating a folder in adagio that only conductor can create files
# and that belongs to the group orchestra
mkdir -p $adagio/conductor
chown conductor $adagio/conductor
chgrp orchestra $adagio/conductor
chmod -R ugo-rxw $adagio/conductor
chmod -R u+rxw $adagio/conductor



exit 0
