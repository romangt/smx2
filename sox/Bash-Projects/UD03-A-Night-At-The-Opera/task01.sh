#!/bin/bash

# users and groups

users=(piccolo clarinet horn trunk fiddle viola cello doublebass battery xylophone conductor)

groups=(strings woodwind metalwind percussion conductor orchestra)


# checking if user exists, if not, create it in an unattended way

for user in "${users[@]}"; do
	if id "$user" >/dev/null; then
		echo "$user user already exists"
	else
		adduser --disabled-password --gecos "" $user >/dev/null
		echo "Creando usuario $user"
	# setting password in unattended way
	echo "$user:unattended_password" | chpasswd
	sleep 1
	fi
done

# checking if group exists, if not, create it

for group in "${groups[@]}"; do
	if getent group "$group" >/dev/null; then
		echo "$group group already exists"
	else
		groupadd "$group"
	fi
done


# adding users into their groups

# piccolo
usermod -a -G woodwind piccolo
usermod -a -G orchestra piccolo
echo "added user piccolo into woodwind, orchestra groups"

# clarinet
usermod -a -G woodwind clarinet
usermod -a -G orchestra clarinet
echo "added user clarinet into woodwind, orchestra groups"

# horn
usermod -a -G metalwind horn
usermod -a -G orchestra horn
echo "added user horn into metalwind, orchestra groups"

# trunk
usermod -a -G metalwind trunk
usermod -a -G orchestra trunk
echo "added user trunk into metalwind, orchestra groups"

# fiddle
usermod -a -G strings fiddle
usermod -a -G orchestra fiddle
echo "added user fiddle into strings, orchestra groups"

# viola
usermod -a -G strings viola 
usermod -a -G orchestra viola 
echo "added user viola into strings, orchestra groups"

# cello
usermod -a -G strings cello
usermod -a -G orchestra cello
echo "added user cello into strings, orchestra groups"

# doublebass
usermod -a -G strings doublebass
usermod -a -G orchestra doublebass
echo "added user doublebass into strings, orchestra groups"

# battery
usermod -a -G percussion battery
usermod -a -G orchestra battery
echo "added user battery into percussion, orchestra groups"

# xylophone
usermod -a -G percussion xylophone 
usermod -a -G orchestra xylophone 
echo "added user xylophone into percussion, orchestra groups"

# conductor
usermod -a -G conductor conductor
usermod -a -G orchestra conductor
echo "added user conductor into conductor, orchestra groups"




exit 0
