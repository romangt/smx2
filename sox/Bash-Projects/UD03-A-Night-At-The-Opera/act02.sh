#!/bin/bash

users=(piccolo clarinet horn trunk fiddle viola cello doublebass battery xylophone conductor)

# checking if paths exist, if not, create it

sinfonieta="/srv/sox/Sinfonietta"
saturn="/srv/sox/Saturn"
unfinished="/srv/sox/Unfinished"
valkyries="/srv/sox/Valkyries"

while true; do

if ! [ -e "$sinfonieta" ]; then
echo "creating path: $sinfonieta"
sleep 1
mkdir -p $sinfonieta

if ! [ -e "$saturn" ]; then
echo "creating path: $saturn"
sleep 1
mkdir -p $saturn

if ! [ -e "$unfinished" ]; then
echo "creating path: $unfinished"
sleep 1
mkdir -p $unfinished

if ! [ -e "$valkyries" ]; then
echo "creating path: $valkyries"
sleep 1
mkdir -p $valkyries
fi
fi
fi
fi

break


done


for user in "${users[@]}"; do

	grupos=$(groups "$user" | tr ":" " " | tr -s " " | cut -d " " -f3)
	
	echo "$user" > $sinfonieta/"$user.txt"
	chown "$user" $sinfonieta/$user.txt
	chgrp "$grupos" $sinfonieta/$user.txt 
        #the conductor can read and write all the scores
        setfacl -m u:conductor:rw $sinfonieta/$user.txt
        chmod o-r $sinfonieta/$user.txt
        chmod g-w $sinfonieta/$user.txt

	echo "$user" > $saturn/"$user.txt"
	chown "$user" $saturn/$user.txt
	chgrp "$grupos" $saturn/$user.txt
        #the conductor can read and write all the scores
        setfacl -m u:conductor:rw $saturn/$user.txt
        chmod o-r $saturn/$user.txt
        chmod g-w $saturn/$user.txt

	echo "$user" > $unfinished/"$user.txt"
	chown "$user" $unfinished/$user.txt
	chgrp "$grupos" $unfinished/$user.txt
        #the conductor can read and write all the scores
        setfacl -m u:conductor:rw $unfinished/$user.txt
        chmod o-r $unfinished/$user.txt
        chmod g-w $unfinished/$user.txt


	# in all the scores of the cavalcade of the Valkyries must be the phrase
	echo "The swift Indian bat happily ate cardillo and kiwi, while the stork played the saxophone behind the straw hut .... 0123456789" > $valkyries/"$user.txt"
	chown "$user" $valkyries/$user.txt
	chgrp "$grupos" $valkyries/$user.txt
        #the conductor can read and write all the scores
        setfacl -m u:conductor:rw $valkyries/$user.txt
        chmod o-r $valkyries/$user.txt
        chmod g-w $valkyries/$user.txt

done

# the wind groups in saturn must be able to rw
for user in "${users[@]}"; do
	if [ "$user" == "piccolo" ] || [ "$user" == "clarinet" ] || [ "$user" == "horn" ] || [ "$user" == "trunk" ]; then
		chmod g+rw $saturn/$user.txt
	fi
done

# in the unfinished all the instruments that beggins with 'v' must be able
# to execute by 'others'


for user in "${users[@]}"; do
	begginv=$(echo $user | grep ^v | wc -l)
	if [ $begginv -eq 1 ]; then
		chmod o+x $unfinished/$user.txt
	fi

done

# in sinfonietta must create the following .txt and change permissions to everyone
# since all the instruments are in the group orchestra, the permissions will be for whole group

touch $sinfonieta/TheSilencio.txt
chgrp orchestra $sinfonieta/TheSilencio.txt
chmod g+rwx $sinfonieta/TheSilencio.txt



exit 0
