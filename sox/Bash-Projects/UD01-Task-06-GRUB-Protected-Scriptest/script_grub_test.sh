#!/bin/bash

# Autor: Raul Roman Gasco
# email: <rauromgas@alu.edu.gva.es

# Licensed under GPLv3 or Higher

echo "Script for testing purposes!"

# Test if dual boot is present

rc=0
os-prober | grep -iq "windows" || rc=1

if [ $rc -eq 0 ]; then
	#Do stuff
	echo "Test if dual boot: OK"
else
	#Other stuff and exit 1
	echo " * [ ERROR ] : Dual boot not found! "
	exit 1
fi

#Part 2= Test if grub-pc is installed

a=0

dpkg -l | grep -iq grub || a=1

if [ $a -eq 0 ]; then

echo "Test if grub-pc is installed: OK"

else

echo "[ Error ] no grub installed"


exit 1

fi


#-----------------------------------------------------------

#Part 3= Test if default user is in the correct file

grep "transistor" /etc/grub.d/00_header > /dev/null
RESULT=$?

if [ $RESULT -eq 0 ]; then
	echo "Test if default user found"
else
	echo "[ Error ] no user found"
exit 1

fi

#Part 4= Test if all the other entries has the correct parameters

a=0
cat /etc/grub.d/10_linux | sed -n 197p | grep -iq unrestricted || a=1

if [ $a -eq 0 ]; then
	echo "Test if entriess correct parameters: OK"
else
	echo "[ ERROR ] Entries not correct"
exit 1
fi




exit=0
