#!/bin/bash

cat MOCK_DATA_EXAM.csv | while read linea; do

	id01=$(echo $linea | cut -d "," -f1)
	firstname=$(echo $linea | cut -d "," -f2)
	ipaddress=$(echo $linea | cut -d "," -f4)
	frequency=$(echo $linea | cut -d "," -f7)
	ipclass=$(echo $linea | cut -d "," -f4 | cut -d "." -f1)
	

#echo $id01 $firstname $ipclass $frequency

if [ $ipclass -gt "127" ] && [ $ipclass -lt "192" ] || [ $ipclass -gt "193" ] && [ $ipclass -lt "224" ]; then

	
	if [ "$frequency" != "" ]; then

		if [ $frequency = "Weekly" ] || [ $frequency = "Monthly" ]; then

		echo "ID: $id01  First name: $firstname IP: $ipaddress Frequency: $frequency"

		fi
	fi
fi



done







exit 0
