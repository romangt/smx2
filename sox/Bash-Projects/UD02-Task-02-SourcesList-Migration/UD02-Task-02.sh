#!/bin/bash

# First we check if we are root or not. If not, then we exit

if [ "$EUID" -ne 0 ]; then
	echo " Please run as root"
	exit 
fi

# We make a backup of our sources list before continue
sudo cp /etc/apt/sources.list /root/sources.list.`date +"%y%m%d-%H%M"`.bak

if [ "$1" = "senia" ]; then

# We uninstall the following packages
sudo dpkg --remove-architecture i386
sudo apt purge appstream command-not-found

echo "deb http://tic.ieslasenia.org/ubuntu jammy main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-updates main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-security main universe restricted multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-backports main universe restricted multiverse" > $HOME/sources.senia

sudo mv $HOME/sources.senia /etc/apt/sources.list

sudo apt update

echo "================================================================="
echo " Your sources list was updated to lasenia and a backup has been made"

else
	if [ "$1" = "home" ]; then
	
	echo "deb http://es.archive.ubuntu.com/ubuntu jammy main universe restricted multiverse
deb http://es.archive.ubuntu.com/ubuntu jammy-updates main universe restricted multiverse
deb http://es.archive.ubuntu.com/ubuntu jammy-security main universe restricted multiverse
deb http://es.archive.ubuntu.com/ubuntu jammy-backports main universe restricted multiverse" > $HOME/sources.home

sudo mv $HOME/sources.home /etc/apt/sources.list

sudo apt update
echo "================================================================="
echo " Your sources list was updated to home and a backup has been made"



fi
fi



exit 0
