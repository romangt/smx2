#!/bin/bash

if [ "$(id -u)" -ne 0 ]; then
	echo "must be root"
	exit 1
fi

ldapsearch -x -b dc=ubuntusrv,dc=smx2023,dc=net -H ldap://10.0.2.20 | grep "ou:" > prueba

cat prueba | cut -d ":" -f2 | while read line; do
	mkdir -p /srv/nfs/invertebrados/$line
done

exit 0
