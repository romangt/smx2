#!/bin/bash

#Use relative path to execute. You may add this script to your current directory

a=$(cat TheHistoryofthePeloponnesianWar.txt | grep $1 | wc -l)

echo "$a Occurences"


if [ $a -eq "0" ]; then

echo "Level : Ourea"

fi

if [ $a -ge 1 ] && [ $a -lt 12 ]; then

echo "Level : Tartarus"

fi

if [ $a -ge 13 ] && [ $a -lt 30 ]; then

echo "Level: Hemera"

fi

if [ $a -ge 31 ] && [ $a -lt 100 ]; then

echo "Level: Nyx"

fi

if [ $a -ge 101 ] && [ $a -lt 300 ]; then

echo "Level : Chronos"

fi

if [ $a -ge 300 ]; then

echo "Level : Chaos"

fi




exit 0

