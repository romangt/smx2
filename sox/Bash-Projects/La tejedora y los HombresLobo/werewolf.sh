#!/bin/bash

#Showing USAGE 
if [ $# -eq 0 ]; then
	echo " [ USAGE ] : $(basename $0) ARGS"
       	exit 1	
fi

#Script must be executed with root permissions
if [ "$EUID" -ne 0 ]; then
	echo " Please run as root"
	exit
fi

#argument "humano" replaces preconfigured grub-humano.cfg
if [ $1 = "humano" ]; then
	rm /boot/grub/grub.cfg
	cp grub-humano.cfg /boot/grub/grub.cfg
fi


#argument "lobo" replaces preconfigured grub-lobo.cfg
if [ $1 = "lobo" ]; then
	rm /boot/grub/grub.cfg
        cp grub-lobo.cfg /boot/grub/grub.cfg
fi




exit 0
