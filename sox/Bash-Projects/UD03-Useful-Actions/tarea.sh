#!/bin/bash

# Check if the correct number of arguments were provided
if [ "$#" -gt 3 ] || [ "$#" -lt 2 ]; then
  echo "*[USAGE] script requires at least 2 arguments"
  echo " 1st argument must be a valid user"
  echo " 2nd argument can be:"
  echo " replenish"
  echo " test <valid path>"
  echo " clean <valid path>"
  exit 1
fi


# Check if the user exists
if ! id "$1" >/dev/null 2>&1; then
  # Display an error message if the user does not exist
  echo "Error - The user $1 does not exist"
  exit 1
fi

# Replenish
if [ $2 = "replenish" ]; then
        mkdir -p /home/$1/SolarSystem/Mercury
        mkdir -p /home/$1/SolarSystem/Venus
        mkdir -p /home/$1/SolarSystem/Mars
        mkdir -p /home/$1/SolarSystem/Jupiter
        mkdir -p /home/$1/SolarSystem/Saturn
        mkdir -p /home/$1/SolarSystem/Uranus
        mkdir -p /home/$1/SolarSystem/Neptune


directorios=$(ls /home/$1/SolarSystem)

echo "Doing stuff..."
    sleep 2

echo "$directorios" | while read line; do
            
    directorios=$(echo $(readlink -f /home/$1/SolarSystem/$line))
    datecreation=$(LANG=C stat   /home/$1/SolarSystem/$line| tr -s " " | grep Birth| cut -d ":" -f2 | cut -d " " -f2)
    
    
    echo "$1 $datecreation" >> $directorios/control-planet.txt
                
                done

                # Get the current date and time in the required format
                datetime="$(date '+%Y%m%d-%H%M')"

                # Deleting control-planet from Saturn and adding the correct format
                if [ -e /home/$1/SolarSystem/Saturn/control-planet.txt ]; then
                  rm -r /home/$1/SolarSystem/Saturn/control-planet.txt
                fi

                echo "Creating file at: $HOME/SolarSystem/Saturn/Environment-$1-$datetime.txt"
                sleep 2
                touch "$HOME/SolarSystem/Saturn/Environment-$1-$datetime.txt"
                echo "All is done"

fi


# Test
if [ "$2" == "test" ]; then
  # Get the path argument
  path="$3"

  # Check if the path exists and is a directory
  if [ -d "$path" ]; then
    # Check if the user has read permission for all the files and directories in the given path
    if [ -r "$path" ]; then
      echo "Test 1: The user $1 exists."
      echo "Test 2: The action $2 is a valid action."
      echo "Test 3: The user $1 has read permission for all the files and directories in $path."
    else
      echo "Error! The user $1 does not have read permission for all the files and directories in $path."
    fi
  else
    echo "Error! The folder given to test does not exist: $path"
  fi
fi


# Clean
if [ "$2" == "clean" ]; then
  # Get the path argument
  path="$3"
  # Check if the path exists and is a directory
  if [ -d "$path" ]; then
  echo "Removing..."
  sleep 2
  rm -r "$path"
  echo "All done"
  else
  echo "Error! The folder given to test does not exist: $path"
  fi
fi

exit 0
