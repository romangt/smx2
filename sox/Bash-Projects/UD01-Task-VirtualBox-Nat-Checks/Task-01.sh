#!/bin/bash

sistemas=$(VBoxManage list vms -l | grep -i "^name:" | tr -d " " | cut -d ":" -f2)

echo "List of all the VBoxMachines configured: 
$sistemas"


exit 0
