#!/bin/bash


VBoxManage list vms | while read vm; do

	echo " * Working with : $vm"
	vmId=$(echo $vm | cut -d "{" -f2 | tr "}" " ")
	
	vmState=$(VBoxManage showvminfo $vmId | grep ^State | tr -s " " )
	echo " * State : $vmState"
	
	#VBoxManage showvminfo $vmId
	nicsEnabled=$(VBoxManage showvminfo $vmId | grep ^NIC | grep -v "disabled" | grep -v "NIC 1 Settings" | tr -s " " | cut -d "," -f1-3 )
	echo " * $nicsEnabled"
	echo " ======== "


done

echo " ****** NAT ******* "
	
portforwarding=$(VBoxManage list natnets)
	
echo "$portforwarding "





exit 0
