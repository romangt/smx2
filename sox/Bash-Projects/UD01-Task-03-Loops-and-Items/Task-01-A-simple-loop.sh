#!/bin/bash

echo "Numbers in descent order"

for n in $(seq 5 -1 1); do
	echo " * ${n} "
done

#-------------------------------

echo "Numbers 1 to 50 in steps of 3"

for n in $(seq 1 3 50); do
        echo " * ${n} "
done

