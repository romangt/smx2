#!/bin/bash

#Añadimos variable que nos coja el argumento num 2 de lo que introducimos al ejecutar el script "ej: /home/smx a
ARG2=$2

#Si el argumento es 0 entonces nos muestra mensaje de cómo usar el script
if [ $# -lt 1 ]; then
echo " USAGE : $(basename $0) PATH_TO_FIND ARG2"
exit 1
fi

#nos busca el argumento introducido en el path que le indicamos y muestra todos los elementos que contienen dicho argumento
for f in $(find ${PATH_TO_FIND} -xtype f); do

rc=0
cat ${f} | grep -q ${ARG2} || rc=1
if [ ${rc} -eq 0 ]; then
echo " * The file ${f} contains ${ARG2}"
fi
done

exit 0
