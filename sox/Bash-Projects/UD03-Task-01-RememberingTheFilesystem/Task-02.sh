#!/bin/bash

if [ $# -eq 0 ]; then
	
	echo " [ USAGE ] : $(basename $0) ARGS"
	
	exit 1

fi
	

path=$1


rc=0

echo $path | grep -q -v "^/" || rc=1


if [ -e $path ]; then

	if [ $rc -eq 0 ]; then
		echo " Path is RELATIVE"
	else
		echo " Path is NOT relative"

	fi
else
	echo " Path DOES NOT exists"

fi



exit 0