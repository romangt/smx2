#!/bin/python3

# https://gitlab.com/romangt

# importing sys library for arguments
import sys
# importing module path
import os

# counting number of arguments (including the main script as an argument)
count = (len(sys.argv))
# checking basename
basename = os.path.basename(sys.argv[0])

if ( count == 1 ):
    print(" [USAGE] : " + basename + " ARGS")

    exit(1)

# Adding argument in a variable
path = sys.argv[1]

# check if path exists
# check if its absolute path
# if "absolute" will return TRUE, if "relative" will return FALSE

if os.path.exists(path):
    if os.path.isabs(path) != True:
        print(" Path is RELATIVE")
    else:
        print(" Path is ABSOLUTE")

else:
    print(" Path does not exists")



exit(0)