#!/bin/python3

# https://gitlab.com/romangt

# Script works either for GNU/Linux and Windows

# importing sys library for arguments
import sys
# importing module path
import os

# counting number of arguments (including the main script as an argument)
count = (len(sys.argv))
# checking basename
basename = os.path.basename(sys.argv[0])

if ( count == 1 ):
    print(" [USAGE] : " + basename + " ARGS")

    exit(1)

# Adding argument in a variable
path = sys.argv[1]

# check if directory or file exists
# if it exists, check if its a file, then check the permissions


if (os.path.isdir(path)) or (os.path.isfile(path)):
    print(" The file/folder exists")
    if os.path.isfile(path):
        if os.access(path, os.R_OK) == True:
            print(" Is a file and the current user has reading permissions")
        else:
            print(" Is a file and the current user HAS NOT reading permissions")
        if os.access(path, os.W_OK) == True:
            print(" Is a file and the current user has writing permissions")
        else:
            print(" Is a file and the current user HAS NOT writing permissions")
        if os.access(path, os.X_OK) == True:
            print(" Is a file and the current user has executing permissions")
        else:
            print(" Is a file and the current user HAS NOT executing permissions")
        
else:
    print(" The file/folder DOES NOT exists")
    exit(1)

exit(0)