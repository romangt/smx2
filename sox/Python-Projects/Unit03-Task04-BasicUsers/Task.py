#!/bin/python3

# importing sys library for arguments
import sys
# importing module path
import os
# importing module for checking users
import pwd
# importing module shutil
import shutil
# importing module pathlib
import pathlib

# counting number of arguments (including the main script as an argument)
count = (len(sys.argv))

# checking basename
basename = os.path.basename(sys.argv[0])

# outputs USAGE if no arguments provided
if ( count == 1 ) or ( count == 2 ):
    print(" [USAGE] : " + basename + " [2]ARGS")
    exit(1)

arg1 = sys.argv[1]
arg2 = sys.argv[2]

# checks if user given in arg1 exist
try:
    pwd.getpwnam(arg1)
except KeyError:
    print(' * Error - The User ' + arg1 + ' does not exists.')
    exit(1)

# REPLENISH MODULE
if ( arg2 == "replenish" ):
    # following line outputs arg1(user) home path
    home = os.path.expanduser('~' + arg1)
    directory = "Workspace"
    # following line creates directory (workspace) in home path
    path = os.path.join(home, directory)
    os.mkdir(path)
    # following lines create the subfolders
    subdir1 = "config"
    subdir2 = "bin"
    subdir3 = "source"
    subdir4 = "rsrc"
    subpath1 = os.path.join(path, subdir1)
    subpath2 = os.path.join(path, subdir2)
    subpath3 = os.path.join(path, subdir3)
    subpath4 = os.path.join(path, subdir4)
    os.mkdir(subpath1)
    os.mkdir(subpath2)
    os.mkdir(subpath3)
    os.mkdir(subpath4)
    os.chdir(subpath1)
    # following lines creates sample.txt (overwriting if needed "w") and prints inside arg1(user)
    with open('sample.txt', 'w') as f:
       print(arg1, file=f)
    os.chdir(subpath2)
    with open('sample.txt', 'w') as f:
       print(arg1, file=f)
    os.chdir(subpath3)
    with open('sample.txt', 'w') as f:
       print(arg1, file=f)
    os.chdir(subpath4)
    with open('sample.txt', 'w') as f:
       print(arg1, file=f)
       exit(0)
       

# TEST MODULE
if ( count == 1 ) or ( count == 2 ) or ( count == 3 ):
    print(" [USAGE] : " + basename + " [3]ARGS")
    exit(1)

if (arg2 == "test" ):
    home = os.path.expanduser('~' + arg1)
    arg3 = sys.argv[3]
    # importing pathlib module into path, python will convert paths cross platforms
    from pathlib import Path
    path = Path(arg3)
    if os.path.exists(path):
        if os.access(path, os.R_OK) == True:
            for root,d_names,f_names in os.walk(path):
                if os.access(root, os.R_OK) == True:
                    print ( '--The folder: ' + root + ' exists and could be read by ' + arg1)

    else:
        print ('* Error - The given path DOES NOT exist')
    
    exit(0)


# CLEAN MODULE
if ( count == 1 ) or ( count == 2 ) or ( count == 3 ):
    print(" [USAGE] : " + basename + " [3]ARGS")
    exit(1)

# checking if path exists and remove it
if ( arg2 == "clean" ):
    arg3 = sys.argv[3]
    # importing pathlib module into path, python will convert paths cross platforms
    from pathlib import Path
    path = Path(arg3)
    if os.path.exists(path):
        shutil.rmtree(path)




exit(0)