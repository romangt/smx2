## Comandos basicos:

git clone "enlace del repositorio" (clonar un repositorio al local)  
git push origin "rama" (enviar cambios a la rama del repositorio remoto)  
git pull (traer cambios del remoto al local)  
git checkout "rama" (cambiarse de rama)  
git merge "rama" (hacer un merge de la rama indicada en la rama actual)  


## Añadir repositorio remoto:

git remote add upstream "enlace repositorio de diego"  
git remote -v (para ver la lista de repositorios remotos)  


## Traer cambios del repositorio remoto al local:

git pull upstream main  
Crear rama --> git branch "nombre rama"  
Enviarlo a mi repo --> Git push origin "rama"  
Merge request marcar opcion delete  
Crear y borrar ramas:  
git branch --> ver rama  
git branch -d -->borrar rama local